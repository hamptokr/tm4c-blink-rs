#![feature(panic_implementation)]
#![no_std]
#![deny(missing_docs)]

//! This crate provides an abstraction over the low level   
//! initilization before the user's `main` function is called

use core::panic::PanicInfo;
use core::ptr;

#[macro_export]
macro_rules! entry {
  ($path:path) => {
    #[export_name = "main"]
    pub unsafe fn __main() -> ! {
      // type check the given path
      let f: fn() -> ! = $path;

      f()
    }
  };
}

/// An Entry into the Vector table
pub union Vector {
  reserved: u32,
  handler: unsafe extern "C" fn(),
}

extern "C" {
  fn NMI();
  fn HardFault();
  fn MPUFault();
  fn BusFault();
  fn UsageFault();
  fn SVCall();
  fn DebugMonitor();
  fn PendSV();
  fn SysTick();
  fn GPIOA();
  fn GPIOB();
  fn GPIOC();
  fn GPIOD();
  fn GPIOE();
  fn UART0();
  fn UART1();
  fn SSI0();
  fn I2C0();
  fn PWMFault();
  fn PWMGen0();
  fn PWMGen1();
  fn PWMGen2();
  fn QuadEnc();
  fn ADC0Seq0();
  fn ADC0Seq1();
  fn ADC0Seq2();
  fn ADC0Seq3();
  fn Watchdog();
  fn Timer0A();
  fn Timer0B();
  fn Timer1A();
  fn Timer1B();
  fn Timer2A();
  fn Timer2B();
  fn AnalogCmp0();
  fn AnalogCmp1();
  fn AnalogCmp2();
  fn SysCtl();
  fn FlashCtl();
  fn GPIOF();
  fn GPIOG();
  fn GPIOH();
  fn UART2();
  fn SSI1();
  fn Timer3A();
  fn Timer3B();
  fn I2C1();
  fn CAN0();
  fn CAN1();
  fn Ethernet();
  fn Hibernate();
  fn USB0Device();
  fn PWMGen3();
  fn DMASoftwareTx();
  fn DMAError();
  fn ADC1Seq0();
  fn ADC1Seq1();
  fn ADC1Seq2();
  fn ADC1Seq3();
  fn ExtBus0();
  fn GPIOJ();
  fn GPIOK();
  fn GPIOL();
  fn SSI2();
  fn SSI3();
  fn UART3();
  fn UART4();
  fn UART5();
  fn UART6();
  fn UART7();
  fn I2C2();
  fn I2C3();
  fn Timer4A();
  fn Timer4B();
  fn Timer5A();
  fn Timer5B();
  fn FPU();
  fn I2C4();
  fn I2C5();
  fn GPIOM();
  fn GPION();
  fn Tamper();
  fn GPIOP0();
  fn GPIOP1();
  fn GPIOP2();
  fn GPIOP3();
  fn GPIOP4();
  fn GPIOP5();
  fn GPIOP6();
  fn GPIOP7();
  fn GPIOQ0();
  fn GPIOQ1();
  fn GPIOQ2();
  fn GPIOQ3();
  fn GPIOQ4();
  fn GPIOQ5();
  fn GPIOQ6();
  fn GPIOQ7();
  fn GPIOR();
  fn GPIOS();
  fn SHAMD5();
  fn AES0();
  fn DES3DES0();
  fn LCD0();
  fn Timer6A();
  fn Timer6B();
  fn Timer7A();
  fn Timer7B();
  fn I2C6();
  fn I2C7();
  fn HIMScanMatrixKeyboard();
  fn OneWire0();
  fn HIMPS20();
  fn HIMLEDSeq0();
  fn HIMConsumerIR0();
  fn I2C8();
  fn I2C9();
  fn GPIOT();
  fn Fan1();
}

/// Vector table layout defined by processor
#[link_section = ".vector_table.exceptions"]
#[no_mangle]
pub static EXCEPTIONS: [Vector; 128] = [
  Vector { handler: NMI },
  Vector { handler: HardFault },
  Vector { handler: MPUFault },
  Vector { handler: BusFault },
  Vector { handler: UsageFault },
  Vector { reserved: 0 },
  Vector { reserved: 0 },
  Vector { reserved: 0 },
  Vector { reserved: 0 },
  Vector { handler: SVCall },
  Vector { handler: DebugMonitor },
  Vector { reserved: 0 },
  Vector { handler: PendSV },
  Vector { handler: SysTick },
  Vector { handler: GPIOA },
  Vector { handler: GPIOB },
  Vector { handler: GPIOC },
  Vector { handler: GPIOD },
  Vector { handler: GPIOE },
  Vector { handler: UART0 },
  Vector { handler: UART1 },
  Vector { handler: SSI0 },
  Vector { handler: I2C0 },
  Vector { handler: PWMFault },
  Vector { handler: PWMGen0 },
  Vector { handler: PWMGen1 },
  Vector { handler: PWMGen2 },
  Vector { handler: QuadEnc },
  Vector { handler: ADC0Seq0 },
  Vector { handler: ADC0Seq1 },
  Vector { handler: ADC0Seq2 },
  Vector { handler: ADC0Seq3 },
  Vector { handler: Watchdog },
  Vector { handler: Timer0A },
  Vector { handler: Timer0B },
  Vector { handler: Timer1A },
  Vector { handler: Timer1B },
  Vector { handler: Timer2A },
  Vector { handler: Timer2B },
  Vector { handler: AnalogCmp0 },
  Vector { handler: AnalogCmp1 },
  Vector { handler: AnalogCmp2 },
  Vector { handler: SysCtl },
  Vector { handler: FlashCtl },
  Vector { handler: GPIOF },
  Vector { handler: GPIOG },
  Vector { handler: GPIOH },
  Vector { handler: UART2 },
  Vector { handler: SSI1 },
  Vector { handler: Timer3A },
  Vector { handler: Timer3B },
  Vector { handler: I2C1 },
  Vector { handler: CAN0 },
  Vector { handler: CAN1 },
  Vector { handler: Ethernet },
  Vector { handler: Hibernate },
  Vector { handler: USB0Device },
  Vector { handler: PWMGen3 },
  Vector { handler: DMASoftwareTx },
  Vector { handler: DMAError },
  Vector { handler: ADC1Seq0 },
  Vector { handler: ADC1Seq1 },
  Vector { handler: ADC1Seq2 },
  Vector { handler: ADC1Seq3 },
  Vector { handler: ExtBus0 },
  Vector { handler: GPIOJ },
  Vector { handler: GPIOK },
  Vector { handler: GPIOL },
  Vector { handler: SSI2 },
  Vector { handler: SSI3 },
  Vector { handler: UART3 },
  Vector { handler: UART4 },
  Vector { handler: UART5 },
  Vector { handler: UART6 },
  Vector { handler: UART7 },
  Vector { handler: I2C2 },
  Vector { handler: I2C3 },
  Vector { handler: Timer4A },
  Vector { handler: Timer4B },
  Vector { handler: Timer5A },
  Vector { handler: Timer5B },
  Vector { handler: FPU },
  Vector { reserved: 0 },
  Vector { reserved: 0 },
  Vector { handler: I2C4 },
  Vector { handler: I2C5 },
  Vector { handler: GPIOM },
  Vector { handler: GPION },
  Vector { reserved: 0 },
  Vector { handler: Tamper },
  Vector { handler: GPIOP0 },
  Vector { handler: GPIOP1 },
  Vector { handler: GPIOP2 },
  Vector { handler: GPIOP3 },
  Vector { handler: GPIOP4 },
  Vector { handler: GPIOP5 },
  Vector { handler: GPIOP6 },
  Vector { handler: GPIOP7 },
  Vector { handler: GPIOQ0 },
  Vector { handler: GPIOQ1 },
  Vector { handler: GPIOQ2 },
  Vector { handler: GPIOQ3 },
  Vector { handler: GPIOQ4 },
  Vector { handler: GPIOQ5 },
  Vector { handler: GPIOQ6 },
  Vector { handler: GPIOQ7 },
  Vector { handler: GPIOR },
  Vector { handler: GPIOS },
  Vector { handler: SHAMD5 },
  Vector { handler: AES0 },
  Vector { handler: DES3DES0 },
  Vector { handler: LCD0 },
  Vector { handler: Timer6A },
  Vector { handler: Timer6B },
  Vector { handler: Timer7A },
  Vector { handler: Timer7B },
  Vector { handler: I2C6 },
  Vector { handler: I2C7 },
  Vector { handler: HIMScanMatrixKeyboard },
  Vector { handler: OneWire0 },
  Vector { handler: HIMPS20 },
  Vector { handler: HIMLEDSeq0 },
  Vector { handler: HIMConsumerIR0 },
  Vector { handler: I2C8 },
  Vector { handler: I2C9 },
  Vector { handler: GPIOT },
  Vector { handler: Fan1 },
  Vector { reserved: 0 },
];

/// Catch all function for unimplemented ISR's
#[no_mangle]
pub extern "C" fn DefaultExceptionHandler() {
  loop {}
}

/// Reset handler for the micro, initializes SRAM for static variables and string literals
#[no_mangle]
pub unsafe extern "C" fn Reset() -> ! {
  // Initialize the SRAM
  extern "C" {
    static mut _sbss: u8;
    static mut _ebss: u8;

    static mut _sdata: u8;
    static mut _edata: u8;
    static _sidata: u8;
  }

  let count = &_ebss as *const u8 as usize - &_sbss as *const u8 as usize;
  ptr::write_bytes(&mut _sbss as *mut u8, 0, count);

  let count = &_edata as *const u8 as usize - &_sdata as *const u8 as usize;
  ptr::copy_nonoverlapping(&_sidata as *const u8, &mut _sdata as *mut u8, count);

  // call the user entry point
  extern "Rust" {
    fn main() -> !;
  }

  main()
}

/// Exposes Reset handler to Linker
#[link_section = ".vector_table.reset_vector"]
#[no_mangle]
pub static RESET_VECTOR: unsafe extern "C" fn() -> ! = Reset;

/// Dummy panic handler
#[panic_implementation]
#[no_mangle]
pub fn panic(_panic: &PanicInfo) -> ! {
  loop {}
}
