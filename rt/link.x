/* Memory layout of the TM4C1294XL microcontroller */
MEMORY
{
  FLASH : ORIGIN = 0x00000000, LENGTH = 1M
  SRAM : ORIGIN = 0x20000000, LENGTH = 256K
}

/* The entry point is the reset handler without this, the linker discards the Reset symbol */
ENTRY(Reset);

EXTERN(RESET_VECTOR);

SECTIONS
{
  .vector_table ORIGIN(FLASH) : ALIGN(4)
  {
    /* First entry: initial stack pointer value, stack is full descending */
    LONG(ORIGIN(SRAM) + LENGTH(SRAM));

    /* Second entry: reset vector */
    KEEP(*(.vector_table.reset_vector));

    KEEP(*(.vector_table.exceptions));

    /* If not user-defined use default */
    PROVIDE(NMI = DefaultExceptionHandler);
    PROVIDE(HardFault = DefaultExceptionHandler);
    PROVIDE(MPUFault = DefaultExceptionHandler);
    PROVIDE(BusFault = DefaultExceptionHandler);
    PROVIDE(UsageFault = DefaultExceptionHandler);
    PROVIDE(SVCall = DefaultExceptionHandler);
    PROVIDE(DebugMonitor = DefaultExceptionHandler);
    PROVIDE(PendSV = DefaultExceptionHandler);
    PROVIDE(SysTick = DefaultExceptionHandler);
    PROVIDE(GPIOA = DefaultExceptionHandler);
    PROVIDE(GPIOB = DefaultExceptionHandler);
    PROVIDE(GPIOC = DefaultExceptionHandler);
    PROVIDE(GPIOD = DefaultExceptionHandler);
    PROVIDE(GPIOE = DefaultExceptionHandler);
    PROVIDE(UART0 = DefaultExceptionHandler);
    PROVIDE(UART1 = DefaultExceptionHandler);
    PROVIDE(SSI0 = DefaultExceptionHandler);
    PROVIDE(I2C0 = DefaultExceptionHandler);
    PROVIDE(PWMFault = DefaultExceptionHandler);
    PROVIDE(PWMGen0 = DefaultExceptionHandler);
    PROVIDE(PWMGen1 = DefaultExceptionHandler);
    PROVIDE(PWMGen2 = DefaultExceptionHandler);
    PROVIDE(QuadEnc = DefaultExceptionHandler);
    PROVIDE(ADC0Seq0 = DefaultExceptionHandler);
    PROVIDE(ADC0Seq1 = DefaultExceptionHandler);
    PROVIDE(ADC0Seq2 = DefaultExceptionHandler);
    PROVIDE(ADC0Seq3 = DefaultExceptionHandler);
    PROVIDE(Watchdog = DefaultExceptionHandler);
    PROVIDE(Timer0A = DefaultExceptionHandler);
    PROVIDE(Timer0B = DefaultExceptionHandler);
    PROVIDE(Timer1A = DefaultExceptionHandler);
    PROVIDE(Timer1B = DefaultExceptionHandler);
    PROVIDE(Timer2A = DefaultExceptionHandler);
    PROVIDE(Timer2B = DefaultExceptionHandler);
    PROVIDE(AnalogCmp0 = DefaultExceptionHandler);
    PROVIDE(AnalogCmp1 = DefaultExceptionHandler);
    PROVIDE(AnalogCmp2 = DefaultExceptionHandler);
    PROVIDE(SysCtl = DefaultExceptionHandler);
    PROVIDE(FlashCtl = DefaultExceptionHandler);
    PROVIDE(GPIOF = DefaultExceptionHandler);
    PROVIDE(GPIOG = DefaultExceptionHandler);
    PROVIDE(GPIOH = DefaultExceptionHandler);
    PROVIDE(UART2 = DefaultExceptionHandler);
    PROVIDE(SSI1 = DefaultExceptionHandler);
    PROVIDE(Timer3A = DefaultExceptionHandler);
    PROVIDE(Timer3B = DefaultExceptionHandler);
    PROVIDE(I2C1 = DefaultExceptionHandler);
    PROVIDE(CAN0 = DefaultExceptionHandler);
    PROVIDE(CAN1 = DefaultExceptionHandler);
    PROVIDE(Ethernet = DefaultExceptionHandler);
    PROVIDE(Hibernate = DefaultExceptionHandler);
    PROVIDE(USB0Device = DefaultExceptionHandler);
    PROVIDE(PWMGen3 = DefaultExceptionHandler);
    PROVIDE(DMASoftwareTx = DefaultExceptionHandler);
    PROVIDE(DMAError = DefaultExceptionHandler);
    PROVIDE(ADC1Seq0 = DefaultExceptionHandler);
    PROVIDE(ADC1Seq1 = DefaultExceptionHandler);
    PROVIDE(ADC1Seq2 = DefaultExceptionHandler);
    PROVIDE(ADC1Seq3 = DefaultExceptionHandler);
    PROVIDE(ExtBus0 = DefaultExceptionHandler);
    PROVIDE(GPIOJ = DefaultExceptionHandler);
    PROVIDE(GPIOK = DefaultExceptionHandler);
    PROVIDE(GPIOL = DefaultExceptionHandler);
    PROVIDE(SSI2 = DefaultExceptionHandler);
    PROVIDE(SSI3 = DefaultExceptionHandler);
    PROVIDE(UART3 = DefaultExceptionHandler);
    PROVIDE(UART4 = DefaultExceptionHandler);
    PROVIDE(UART5 = DefaultExceptionHandler);
    PROVIDE(UART6 = DefaultExceptionHandler);
    PROVIDE(UART7 = DefaultExceptionHandler);
    PROVIDE(I2C2 = DefaultExceptionHandler);
    PROVIDE(I2C3 = DefaultExceptionHandler);
    PROVIDE(Timer4A = DefaultExceptionHandler);
    PROVIDE(Timer4B = DefaultExceptionHandler);
    PROVIDE(Timer5A = DefaultExceptionHandler);
    PROVIDE(Timer5B = DefaultExceptionHandler);
    PROVIDE(FPU = DefaultExceptionHandler);
    PROVIDE(I2C4 = DefaultExceptionHandler);
    PROVIDE(I2C5 = DefaultExceptionHandler);
    PROVIDE(GPIOM = DefaultExceptionHandler);
    PROVIDE(GPION = DefaultExceptionHandler);
    PROVIDE(Tamper = DefaultExceptionHandler);
    PROVIDE(GPIOP0 = DefaultExceptionHandler);
    PROVIDE(GPIOP1 = DefaultExceptionHandler);
    PROVIDE(GPIOP2 = DefaultExceptionHandler);
    PROVIDE(GPIOP3 = DefaultExceptionHandler);
    PROVIDE(GPIOP4 = DefaultExceptionHandler);
    PROVIDE(GPIOP5 = DefaultExceptionHandler);
    PROVIDE(GPIOP6 = DefaultExceptionHandler);
    PROVIDE(GPIOP7 = DefaultExceptionHandler);
    PROVIDE(GPIOQ0 = DefaultExceptionHandler);
    PROVIDE(GPIOQ1 = DefaultExceptionHandler);
    PROVIDE(GPIOQ2 = DefaultExceptionHandler);
    PROVIDE(GPIOQ3 = DefaultExceptionHandler);
    PROVIDE(GPIOQ4 = DefaultExceptionHandler);
    PROVIDE(GPIOQ5 = DefaultExceptionHandler);
    PROVIDE(GPIOQ6 = DefaultExceptionHandler);
    PROVIDE(GPIOQ7 = DefaultExceptionHandler);
    PROVIDE(GPIOR = DefaultExceptionHandler);
    PROVIDE(GPIOS = DefaultExceptionHandler);
    PROVIDE(SHAMD5 = DefaultExceptionHandler);
    PROVIDE(AES0 = DefaultExceptionHandler);
    PROVIDE(DES3DES0 = DefaultExceptionHandler);
    PROVIDE(LCD0 = DefaultExceptionHandler);
    PROVIDE(Timer6A = DefaultExceptionHandler);
    PROVIDE(Timer6B = DefaultExceptionHandler);
    PROVIDE(Timer7A = DefaultExceptionHandler);
    PROVIDE(Timer7B = DefaultExceptionHandler);
    PROVIDE(I2C6 = DefaultExceptionHandler);
    PROVIDE(I2C7 = DefaultExceptionHandler);
    PROVIDE(HIMScanMatrixKeyboard = DefaultExceptionHandler);
    PROVIDE(OneWire0 = DefaultExceptionHandler);
    PROVIDE(HIMPS20 = DefaultExceptionHandler);
    PROVIDE(HIMLEDSeq0 = DefaultExceptionHandler);
    PROVIDE(HIMConsumerIR0 = DefaultExceptionHandler);
    PROVIDE(I2C8 = DefaultExceptionHandler);
    PROVIDE(I2C9 = DefaultExceptionHandler);
    PROVIDE(GPIOT = DefaultExceptionHandler);
    PROVIDE(Fan1 = DefaultExceptionHandler);
  } > FLASH

  .text :
  {
    *(.text .text.*);
  } > FLASH

  .rodata :
  {
    *(.rodata .rodata.*);
  } > FLASH

  .bss :
  {
    _sbss = .;
    *(.bss .bss.*);
    _ebss = .;
  } > SRAM

  .data : AT(ADDR(.rodata) + SIZEOF(.rodata))
  {
    _sdata = .;
    *(.data .data.*);
    _edata = .;
  } > SRAM

  _sidata = LOADADDR(.data);

  /DISCARD/ :
  {
    *(.ARM.exidx.*);
  }
}