#![feature(core_intrinsics)]
#![no_std]
#![no_main]

#[macro_use]
extern crate rt;

use core::ptr;

entry!(main);

// SysCtl
const SYSCTL_RCGCTIMER_R: u32 = 0x400FE604;
const SYSCTL_RCGCGPIO_R: u32 = 0x400FE608;
// GPIO Port N
const GPIO_PORTN_DIR_R: u32 = 0x40064400;
const GPIO_PORTN_DEN_R: u32 = 0x4006451C;
const GPIO_PORTN_DATA_R: u32 = 0x400643FC;
// GPIO Port F AHB
const GPIO_PORTF_AHB_DIR_R: u32 = 0x4005D400;
const GPIO_PORTF_AHB_DEN_R: u32 = 0x4005D51C;
const GPIO_PORTF_AHB_DATA_R: u32 = 0x4005D3FC;
//Timer 0
const TIMER0_CTL_R: u32 = 0x4003000C;
const TIMER0_CFG_R: u32 = 0x40030000;
const TIMER0_TAMR_R: u32 = 0x40030004;
const TIMER0_TAILR_R: u32 = 0x40030028;
const TIMER0_RIS_R: u32 = 0x4003001C;
const TIMER0_ICR_R: u32 = 0x40030024;

fn main() -> ! {
    unsafe {
        // Enable GPIO Port N and F, 2 user LED's each
        ptr::write_volatile(SYSCTL_RCGCGPIO_R as *mut u32, 0x13A4);

        // Set the GPIO Direction to output and enable for digital function
        ptr::write_volatile(GPIO_PORTN_DIR_R as *mut u32, 0x03);
        ptr::write_volatile(GPIO_PORTN_DEN_R as *mut u32, 0x03);
        ptr::write_volatile(GPIO_PORTF_AHB_DIR_R as *mut u32, 0x11);
        ptr::write_volatile(GPIO_PORTF_AHB_DEN_R as *mut u32, 0x11);

        // Enable the timer's register
        ptr::write_volatile(SYSCTL_RCGCTIMER_R as *mut u32, 0x01);

        // Takes a few clock cycles before timer is ready
        while ptr::read_volatile(SYSCTL_RCGCTIMER_R as *const u32) != 0x01 {}

        // Ensure the timer is disabled
        ptr::write_volatile(TIMER0_CTL_R as *mut u32, 0x00);

        // Configure a 32-bit timer
        ptr::write_volatile(TIMER0_CFG_R as *mut u32, 0x00);

        // Set to periodic mode
        ptr::write_volatile(TIMER0_TAMR_R as *mut u32, 0x02);

        // Set the value of the timer to 16,000,000 (Default clock speed 16MHz)
        ptr::write_volatile(TIMER0_TAILR_R as *mut u32, 0xF42400);

        // Enable the timer and start counting
        ptr::write_volatile(TIMER0_CTL_R as *mut u32, 0x01);
    }

    loop {
        unsafe {
            // Poll the timer bit to see if time is up
            if ptr::read_volatile(TIMER0_RIS_R as *const u32) == 0x01 {
                // clear the bit when fired
                ptr::write_volatile(TIMER0_ICR_R as *mut u32, 0x01);
                toggle_leds();
            }
        }
    }
}

unsafe fn toggle_leds() -> () {
    if ptr::read_volatile(GPIO_PORTN_DATA_R as *const u32) == 0x03 && ptr::read_volatile(GPIO_PORTF_AHB_DATA_R as *const u32) == 0x11 {
        ptr::write_volatile(GPIO_PORTN_DATA_R as *mut u32, 0x00);
        ptr::write_volatile(GPIO_PORTF_AHB_DATA_R as *mut u32, 0x00);
    } else {
        ptr::write_volatile(GPIO_PORTN_DATA_R as *mut u32, 0x03);
        ptr::write_volatile(GPIO_PORTF_AHB_DATA_R as *mut u32, 0x11);
    }

    ()
}
