# tm4c-blink-rs

Learning Rust for the TM4C129

## Architecture
This repo consists of 2 projects, `rt` which is the runtime, and `application` which is where the user code lies. The `rt` library crate handles the linking, RAM initialization, and everything else that happens before `main` is called. This is so that the `application` binary is _only_ application logic.

## Documentation
To generate the documentation for this application simply run `cargo doc --open`

## Toolchain Setup
Get rustup from [here](https://rustup.rs) if you do not have it already.   
```bash
# Install the necessary tools
rustup default nightly
rustup target add thumbv7m-none-eabi
cargo install cargo-binutils
rustup component add llvm-tools-preview
sudo apt-get install libssl-dev # For debian as an example
sudo pacman -S openssl # For arch as another example
cargo install cargo-edit
```


## Building `application`
```bash   
cd application
# Compiling
cargo build # no optimization
cargo build --release # optimizations

# Inspect the output binary
cargo objdump -- -d -no-show-raw-insn target/thumbv7m-none-eabi/debug/application
```

## Debugging on the TM4C
- You must have OpenOCD and GDB   

```bash
cd application
# Running debug version
cargo run
# Running release version
cargo run --release
```   

## Statistics   
```bash   
# Cargo binutils (gcc binutils wrapper)
cargo size -- target/thumbv7m-none-eabi/release/application
   text    data     bss     dec     hex filename
    382     520       0     902     386 target/thumbv7m-none-eabi/release/application
```   

902 bytes!